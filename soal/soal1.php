<?php

 $host = "localhost";  //Nama Host
 $user = "root"; //Nama User
 $pass = ""; //Password
 $db = "data_tes_arkademy"; //Nama Database

 //Syntax MySql untuk melihat semua record yang
 //ada di tabel soal1
 $sql = "SELECT * FROM soal1";
 $link = mysqli_connect($host, $user, $pass, $db) or die($link);

 //Execetute Query diatas
 $query = mysqli_query($link, $sql);

 while($dt=mysqli_fetch_array($query)){
  $item[] = array(
   "itemId"=>$dt["itemId"],
   "itemName"=>$dt["itemName"],
   "price"=>$dt["price"],
   "availableColorAndSize"=> array($dt["availableColorAndSize"]),
   "freeShiping"=>$dt["freeShiping"],
  );
 }

 //Menampung data yang dihasilkan
 $json = array(
    'result' => 'Success',
    'item' => $item
   );

 //Merubah data kedalam bentuk JSON
 echo json_encode($json);
?>
